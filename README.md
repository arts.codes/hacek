# Hacek

háček presents a physical installation, VR experience, and printed data maps. It employs data to inform an immersive installation while positioning it’s larger impact towards metaphors of networked landscape, security and wayfinding.